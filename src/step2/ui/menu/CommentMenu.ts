import { question } from 'readline-sync';
import CommentConsole from '../console/CommentConsole';


class CommentMenu {
  //
  commentConsole: CommentConsole;

  constructor() {
    //
    this.commentConsole = new CommentConsole();
  }

  showMenu(): void {
    //
    let inputNumber = 0;

    while (true) {
      //
      this.displayMainMenu();
      inputNumber = this.selectMenu();

      switch (inputNumber) {
        //
        case 1:
          this.commentConsole.register();
          break;
        case 2:
          this.commentConsole.findAll();
          break;
        case 3:
          this.commentConsole.modify();
          break;
        case 4:
          this.commentConsole.remove();
          break;
        case 0:
          return;

        default:
          console.log('Choose Again!');
      }
    }
  }

  displayMainMenu(): void {
    //
    console.log('................................');
    console.log(' [Comment Menu]');
    console.log('................................');
    console.log(' 1. Register a comment');
    console.log(' 2. Find All comments');
    console.log(' 3. Modify comment');
    console.log(' 4. Remove comment');
    console.log('................................');
    console.log(' 0. Previous');
    console.log('................................');
  }

  selectMenu(): number {
    //
    const answer = question('Select number : ');
    const menuNumber = parseInt(answer);

    if (menuNumber >= 0 && menuNumber <= 4) {
      return menuNumber;
    }
    else {
      console.log('It\'s a invalid number -> ' + menuNumber);
      return -1;
    }
  }

}

export default CommentMenu;
