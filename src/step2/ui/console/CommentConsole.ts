class CommentConsole {

  constructor() {
    //
  }

  register(): void {
    //
    console.log('\n You\'ve select the comment register menu.');
  }

  findAll(): void {
    //
    console.log('\n You\'ve select the comment findAll menu.');
  }

  modify(): void {
    //
    console.log('\n You\'ve select the comment modify menu.');
  }

  remove(): void {
    //
    console.log('\n You\'ve select the comment remove menu.');
  }

}

export default CommentConsole;
