import ClubStoreMapLycler from '../da.map/ClubStoreMapLycler';
import MemberDto from '../service/dto/MemberDto';
import MemberService from '../service/MemberService';
import MemberStore from '../store/MemberStore';
import ClubMembershipDto from '../../step3/service/dto/ClubMembershipDto';
import CommunityMember from '../../step1/entity/club/CommunityMember';
import ClubMembership from '../../step1/entity/club/ClubMembership';
import ClubStore from '../store/ClubStore';


class MemberServiceLogic implements MemberService {
  //
  clubStore: ClubStore;
  memberStore: MemberStore;

  constructor() {
    //
    this.clubStore = ClubStoreMapLycler.getInstance().requestClubStore();
    this.memberStore = ClubStoreMapLycler.getInstance().requestMemberStore();
  }

  register(memberDto: MemberDto): void {
    //
    const email = memberDto.email;
    const foundMember = this.memberStore.retrieve(email);

    if (foundMember) {
      throw new Error('Member already exist the member email --> ' + foundMember.email);
    }
    this.memberStore.create(memberDto.toMember());
  }

  find(memberEmail: string): MemberDto {
    //
    const foundMember = this.memberStore.retrieve(memberEmail);

    if (!foundMember) {
      throw new Error('No such member with email --> ' + memberEmail);
    }
    return MemberDto.fromEntity(foundMember);
  }

  findByName(memberName: string): MemberDto[] {
    //
    const members = this.memberStore.retrieveByName(memberName);

    if (!members) {
      throw new Error('No such member with name --> ' + memberName);
    }

    return members.filter(member => member.name === memberName)
      .map((targetMember) => MemberDto.fromEntity(targetMember));
  }

  modify(memberDto: MemberDto): void {
    //
    const targetMember = this.memberStore.retrieve(memberDto.email);

    if (!targetMember) {
      throw new Error('No such member with email --> ' + memberDto.email);
    }

    if (!memberDto.name) {
      memberDto.name = targetMember.name;
    }

    if (!memberDto.nickName) {
      memberDto.nickName = targetMember.nickName;
    }

    if (!memberDto.phoneNumber) {
      memberDto.phoneNumber = targetMember.phoneNumber;
    }

    if (!memberDto.birthDay) {
      memberDto.birthDay = targetMember.birthDay;
    }

    this.memberStore.update(memberDto.toMember());
  }

  remove(memberEmail: string): void {
    //
    const foundMember = this.memberStore.retrieve(memberEmail);

    if (!foundMember) {
      throw new Error('No such member with email --> ' + memberEmail);
    }

    const membershipListOfClub = this.findAllMemberships(memberEmail);

    this.removeAllMembershipsOfClub(memberEmail, membershipListOfClub);

    this.memberStore.delete(memberEmail);
  }

  findAllMemberships(memberEmail: string): ClubMembershipDto[] {
    //
    const memberships = this.memberStore.retrieve(memberEmail)?.membershipList;

    if (!memberships) {
      return [];
    }

    return memberships.map(membership => ClubMembershipDto.fromEntity(membership));
  }

  removeAllMembershipsOfClub(memberId: string, clubMembershipDtos: ClubMembershipDto[]): void {
    //
    const clubs = clubMembershipDtos.map(membership => membership.clubId);

    for (const club of clubs) {
      const foundClub = this.clubStore.retrieve(club);

      if (foundClub) {
        const targetMembership = foundClub.membershipList.find(membership => membership.memberEmail === memberId);

        foundClub.membershipList = foundClub.membershipList.filter(membership => membership !== targetMembership);
      }
    }
  }

  private getMembershipOfMember(clubId: string, member: CommunityMember): ClubMembership {
    //
    for (const membership of member.membershipList) {
      if (clubId === membership.clubId) {

        return membership;
      }
    }
    throw new Error(`No such club[${clubId}] in member [${member.email}]`);
  }

}

export default MemberServiceLogic;
