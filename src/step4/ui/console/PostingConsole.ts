import { question } from 'readline-sync';
import ServiceLogicLycler from '../../logic/ServiceLogicLycler';
import BoardService from '../../service/BoardService';
import BoardDto from '../../service/dto/BoardDto';
import PostingDto from '../../service/dto/PostingDto';
import PostingService from '../../service/PostingService';
import CommentMenu from '../menu/CommentMenu';


class PostingConsole {
  //
  boardService: BoardService;
  postingService: PostingService;

  constructor() {
    //
    this.boardService = ServiceLogicLycler.shareInstance().createBoardService();
    this.postingService = ServiceLogicLycler.shareInstance().createPostingService();
  }

  register(board: BoardDto): void {
    //
    while (true) {
      const title = question('\n posting title (0.Posting menu): ');

      if (title === '0') {
        return;
      }
      const writerEmail = question(' posting writerEmail: ');
      const contents = question(' posting contents: ');

      try {
        const postingDto = new PostingDto(title, writerEmail, contents);

        postingDto.usid = this.postingService.register(board.clubId, postingDto);
        console.log('\n> Registered a posting --> ', postingDto);
      }
      catch (e) {
        if (e instanceof Error) {
          console.error(`Error: ${e.message}`);
        }
      }
    }
  }

  findByBoardId(board: BoardDto): void {
    //
    const postings = this.postingService.findByBoardId(board.clubId);

    let inputNumber = 0;

    console.log('................................');
    console.log(' < Posting List >');

    if (!postings.length) {
      console.log();
      console.log(`no posting in the ${board.name}`);
      console.log();
    }

    for (const index in postings) {
      const menuNumber = parseInt(index) + 1;

      console.log('   ' + menuNumber + '. ' + postings[index].postingDtoInfo);
    }
    console.log('................................');
    console.log(' 0. Previous');
    console.log('................................');

    inputNumber = this.selectPostingNumber(postings.length);

    if (inputNumber === 0) {
      return;
    }

    const selectPosting = postings[inputNumber - 1];
    const commentMenu = new CommentMenu(selectPosting);

    commentMenu.showMenu();
  }

  find(board: BoardDto): void {
    //
    if (!board) {
      //
      console.log('> No target club yet. Find target club first.');
      return;
    }

    let postingDto = null;

    while (true) {
      const postingId = question('\n posting id to find (0.Posting menu): ');

      if (postingId === '0') {
        break;
      }

      try {
        postingDto = this.postingService.find(postingId);
        console.log('\n> Found posting: ', postingDto);
      }
      catch (e) {
        if (e instanceof Error) {
          console.error(`Error: ${e.message}`);
        }
      }
    }
  }

  findOne(): PostingDto | null {
    //
    let postingDto = null;

    while (true) {
      const postingId = question('\n posting id to find (0.Posting menu): ');

      if (postingId === '0') {
        break;
      }

      try {
        postingDto = this.postingService.find(postingId);
        console.log('\n> Found posting: ', postingDto);
        break;
      }
      catch (e) {
        if (e instanceof Error) {
          console.error(`Error: ${e.message}`);
        }
      }
      postingDto = null;
    }
    return postingDto;
  }

  modify(): void {
    //
    const targetPosting = this.findOne();

    if (!targetPosting) {
      return;
    }

    const newTitle = question('\n new posting title (0.Posting menu, Enter. no change): ');

    if (newTitle === '0') {
      return;
    }
    targetPosting.title = newTitle;

    const contents = question(' new posting contents (Enter. no change): ');

    targetPosting.contents = contents;

    try {
      this.postingService.modify(targetPosting);
      console.log('\n> Modified Posting : ', targetPosting);
    }
    catch (e) {
      if (e instanceof Error) {
        console.error(`Error: ${e.message}`);
      }
    }
  }

  remove(): void {
    //
    const targetPosting = this.findOne();

    if (!targetPosting) {
      return;
    }

    const confirmStr = question('Remove this posting in the board? (Y:yes, N:no): ');

    if (confirmStr.toLowerCase() === 'y' || confirmStr.toLowerCase() === 'yes') {
      //
      console.log('\n> Removing a posting -->' + targetPosting.title);
      this.postingService.remove(targetPosting.usid);
    }
    else {
      console.log('\n> Remove cancelled, the posting is safe --> ' + targetPosting.title);
    }
  }

  selectPostingNumber(postingSize: number): number {
    //
    const answer = question('Select Posting number : ');
    const postingNumber = parseInt(answer);

    if (postingNumber >= 0 && postingNumber <= postingSize) {
      return postingNumber;
    }
    else {
      console.log('it\'s a invalid number -> ' + postingNumber);
      return -1;
    }
  }

}

export default PostingConsole;
