import { question } from 'readline-sync';
import CommentService from '../../service/CommentService';
import ServiceLogicLycler from '../../logic/ServiceLogicLycler';
import CommentDto from '../../service/dto/CommentDto';
import PostingService from '../../service/PostingService';
import PostingDto from '../../service/dto/PostingDto';


class CommentConsole {
  //
  postingService: PostingService;
  commentService: CommentService;

  constructor() {
    //
    this.postingService = ServiceLogicLycler.shareInstance().createPostingService();
    this.commentService = ServiceLogicLycler.shareInstance().createCommentService();
  }

  register(posting: PostingDto): void {
    //
    if (!posting) {
      //
      console.log('\n> No target posting yet. Find target posting first.');
    }

    while (true) {
      const writer = question('\n comment writer (0.Posting menu): ');

      if (writer === '0') {
        return;
      }
      const contents = question(' comment contents: ');

      try {
        const commentDto = new CommentDto(writer, contents);

        commentDto.usid = this.commentService.register(posting.usid, commentDto);
        console.log('\n> Registered a comment --> ', commentDto);
      }
      catch (e) {
        if (e instanceof Error) {
          console.error(`Error: ${e.message}`);
        }
      }
    }
  }

  findAll(posting: PostingDto): void {
    //
    if (!posting) {
      //
      console.log('\n> No target posting yet. Find target posting first.');
      return;
    }

    const comments = this.commentService.findByPostingId(posting.usid);

    console.log('......................');
    console.log(' < Comment List >');

    if (!comments.length) {
      console.log();
      console.log(`no comment in the ${posting.title}`);
      console.log();
    }

    for (const index in comments) {
      const menuNumber = parseInt(index) + 1;
      const comment = comments[index];

      console.log('   ' + menuNumber + '. ' + comment.contents + '  ' + comment.writer + '  ' + comment.writtenDate);
    }
    console.log('......................');
    console.log(' 0. Previous');
  }

}

export default CommentConsole;
