import { question } from 'readline-sync';
import PostingDto from '../../service/dto/PostingDto';
import CommentConsole from '../console/CommentConsole';


class CommentMenu {
  //
  currentPosting: PostingDto;

  commentConsole: CommentConsole;

  constructor(inputPosting: PostingDto) {
    //
    this.currentPosting = inputPosting;
    this.commentConsole = new CommentConsole();
  }

  showMenu(): void {
    //
    let inputNumber = 0;

    while (true) {
      //
      this.displayMainMenu();
      inputNumber = this.selectMenu();

      switch (inputNumber) {
        //
        case 1:
          this.commentConsole.register(this.currentPosting);
          break;
        case 2:
          this.commentConsole.findAll(this.currentPosting);
          break;
        case 0:
          return;

        default:
          console.log('Choose Again!');
      }
    }
  }

  displayMainMenu(): void {
    //
    console.log('......................');
    console.log(' [Comment Menu] ');
    console.log('......................');
    console.log(' 1. Register a comment');
    console.log(' 2. Find all comments');
    console.log('......................');
    console.log(' 0. Previous');
    console.log('......................');
  }

  selectMenu(): number {
    //
    const answer = question('Select number : ');
    const menuNumber = parseInt(answer);

    if (menuNumber >= 0 && menuNumber <= 2) {
      return menuNumber;
    }
    else {
      console.log('it\'s a invalid number -> ' + menuNumber);
      return -1;
    }
  }

}

export default CommentMenu;
