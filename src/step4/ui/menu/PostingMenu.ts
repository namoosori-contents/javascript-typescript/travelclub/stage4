import { question } from 'readline-sync';
import PostingConsole from '../console/PostingConsole';
import BoardDto from '../../service/dto/BoardDto';


class PostingMenu {
  //
  currentBoard: BoardDto;

  postingConsole: PostingConsole;

  constructor(inputBoard: BoardDto) {
    //
    this.currentBoard = inputBoard;
    this.postingConsole = new PostingConsole();
  }

  showMenu(): void {
    //
    let inputNumber = 0;

    while (true) {
      //
      this.displayMainMenu();
      inputNumber = this.selectMenu();

      switch (inputNumber) {
        //
        case 1:
          this.postingConsole.register(this.currentBoard);
          break;
        case 2:
          this.postingConsole.find(this.currentBoard);
          break;
        case 3:
          this.postingConsole.findByBoardId(this.currentBoard);
          break;
        case 4:
          this.postingConsole.modify();
          break;
        case 5:
          this.postingConsole.remove();
          break;
        case 0:
          return;

        default:
          console.log('Choose Again!');
      }
    }
  }

  displayMainMenu(): void {
    //
    console.log('\n');
    console.log('................................');
    console.log(` [Posting Menu for ${this.currentBoard.name}]`);
    console.log('................................');
    console.log(' 1. Register a posting');
    console.log(' 2. Find a posting');
    console.log(' 3. Find postings in the board');
    console.log(' 4. Modify posting');
    console.log(' 5. Remove posting');
    console.log('................................');
    console.log(' 0. Previous');
    console.log('................................');
  }

  selectMenu(): number {
    //
    const answer = question('Select number : ');
    const menuNumber = parseInt(answer);

    if (menuNumber >= 0 && menuNumber <= 5) {
      return menuNumber;
    }
    else {
      console.log('it\'s a invalid number -> ' + menuNumber);
      return -1;
    }
  }

}

export default PostingMenu;
