import TravelClubDto from './service/dto/TravelClubDto';
import MemberDto from './service/dto/MemberDto';
import ClubMembershipDto from './service/dto/ClubMembershipDto';
import ServiceLogicLycler from './logic/ServiceLogicLycler';
import MainMenu from './ui/menu/MainMenu';
import BoardDto from './service/dto/BoardDto';


const sampleClub = new TravelClubDto('test club', 'Welcome to test club.');
const sampleMember = new MemberDto('test@test.co.kr', 'Minsoo Lee', '010-3321-1001');
const sampleMembership = new ClubMembershipDto('0', 'test@test.co.kr');
const sampleBoard = new BoardDto('0', 'test board', 'test@test.co.kr');

const lycler = ServiceLogicLycler.shareInstance();
const clubService = lycler.createClubService();
const memberService = lycler.createMemberService();
const boardService = lycler.createBoardService();

clubService.register(sampleClub);
memberService.register(sampleMember);
clubService.addMembership(sampleMembership);
boardService.register(sampleBoard);

const mainMenu = new MainMenu();

mainMenu.showMenu();
