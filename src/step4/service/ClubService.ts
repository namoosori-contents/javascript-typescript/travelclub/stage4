import ClubMembershipDto from './dto/ClubMembershipDto';
import TravelClubDto from './dto/TravelClubDto';
import TravelClub from '../../step1/entity/club/TravelClub';


interface ClubService {
  //
  register(clubDto: TravelClubDto): void;
  find(clubId: string): TravelClubDto;
  findByName(name: string): TravelClubDto;
  modify(clubDto: TravelClubDto): void;
  remove(clubId: string): void;

  addMembership(membershipDto: ClubMembershipDto): void;
  findMembership(clubId: string, memberId: string): ClubMembershipDto | null;
  findAllMemberships(clubId: string): ClubMembershipDto[];
  modifyMembership(clubId: string, membershipDto: ClubMembershipDto): void;
  removeMembership(clubId: string, memberId: string): void;
  removeAllMembershipsOfMember(clubId: string, clubMembershipDtos: ClubMembershipDto[]): void;
  checkPresidentRole(club: TravelClub, memberId: string): void;
}

export default ClubService;
