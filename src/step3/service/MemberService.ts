import MemberDto from './dto/MemberDto';
import ClubMembershipDto from './dto/ClubMembershipDto';


interface MemberService {
  //
  register(memberDto: MemberDto): void;
  find(memberId: string): MemberDto;
  findByName(memberName: string): MemberDto[];
  modify(memberDto: MemberDto): void;
  remove(memberId: string): void;

  findAllMemberships(memberId: string): ClubMembershipDto[];
  removeAllMembershipsOfClub(memberId: string, clubMembershipDtos: ClubMembershipDto[]): void;
}

export default MemberService;
