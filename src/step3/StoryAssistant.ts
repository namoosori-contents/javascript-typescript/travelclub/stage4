import SocialBoard from '../step1/entity/board/SocialBoard';
import CommunityMember from '../step1/entity/club/CommunityMember';
import TravelClub from '../step1/entity/club/TravelClub';
import MapStorage from './logic/storage/MapStorage';
import MainMenu from './ui/menu/MainMenu';
import TravelClubDto from './service/dto/TravelClubDto';
import MemberDto from './service/dto/MemberDto';
import ServiceLogicLycler from './logic/ServiceLogicLycler';
import ClubMembershipDto from './service/dto/ClubMembershipDto';
import BoardDto from './service/dto/BoardDto';


MapStorage.getInstance().clubMap.set(TravelClub.getSample(true).getId(), TravelClub.getSample(true));
MapStorage.getInstance().memberMap.set(CommunityMember.getSample().email, CommunityMember.getSample());
MapStorage.getInstance().boardMap.set(SocialBoard.getSample(TravelClub.getSample(true)).getId(), SocialBoard.getSample(TravelClub.getSample(true)));

const sampleClub = new TravelClubDto('test club', 'Welcome to test club.');
const sampleMember = new MemberDto('test@test.co.kr', 'Minsoo Lee', '010-3321-1001');
const sampleMembership = new ClubMembershipDto('0', 'test@test.co.kr');
const sampleBoard = new BoardDto('0', 'test board', 'test@test.co.kr');

const lycler = ServiceLogicLycler.shareInstance();
const clubService = lycler.createClubService();
const memberService = lycler.createMemberService();
const boardService = lycler.createBoardService();

clubService.register(sampleClub);
memberService.register(sampleMember);
clubService.addMembership(sampleMembership);
boardService.register(sampleBoard);

const mainMenu = new MainMenu();

mainMenu.showMenu();

