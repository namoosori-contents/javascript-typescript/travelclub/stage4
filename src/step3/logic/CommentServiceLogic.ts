import CommentService from '../service/CommentService';
import CommentDto from '../service/dto/CommentDto';
import Posting from '../../step1/entity/board/Posting';
import Comment from '../../step1/entity/board/Comment';
import MapStorage from './storage/MapStorage';
import SocialBoard from '../../step1/entity/board/SocialBoard';
import TravelClub from '../../step1/entity/club/TravelClub';


class CommentServiceLogic implements CommentService {
  //
  clubMap: Map<string, TravelClub>;
  boardMap: Map<string, SocialBoard>;
  postingMap: Map<string, Posting>;
  commentMap: Map<string, Comment>;

  constructor() {
    //
    this.clubMap = MapStorage.getInstance().clubMap;
    this.boardMap = MapStorage.getInstance().boardMap;
    this.postingMap = MapStorage.getInstance().postingMap;
    this.commentMap = MapStorage.getInstance().commentMap;
  }

  register(postingId: string, commentDto: CommentDto): string {
    //
    const foundPosting = this.postingMap.get(postingId);

    if (!foundPosting) {
      throw new Error('No such posting with id --> ' + postingId);
    }

    const foundBoard = this.boardMap.get(foundPosting.boardId);

    if (!foundBoard) {
      throw new Error('No such posting with id --> ' + postingId);
    }

    const foundClub = this.clubMap.get(foundBoard.clubId);

    if (!foundClub) {
      throw new Error('No such posting with id --> ' + postingId);
    }

    const membership = foundClub.getMembershipBy(commentDto.writer);

    if (!membership) {
      throw new Error('In the club, No such member with admin\'s email --> ' + commentDto.writer);
    }

    const newComment = commentDto.toCommentInPosting(foundPosting);

    this.commentMap.set(newComment.getId(), newComment);

    return newComment.getId();
  }

  find(commentId: string): CommentDto {
    //
    const foundComment = this.commentMap.get(commentId);

    if (!foundComment) {
      throw new Error('No such comment with id --> ' + commentId);
    }
    return CommentDto.fromEntity(foundComment);
  }

  findByPostingId(postingId: string): CommentDto[] {
    //
    const foundPosting = this.postingMap.get(postingId);

    if (!foundPosting) {
      throw new Error('No such posting with id --> ' + postingId);
    }

    const comments = Array.from(this.commentMap.values());

    return comments.filter(comment => comment.postingId === postingId)
      .map(targetComment => CommentDto.fromEntity(targetComment));
  }

  modify(commentDto: CommentDto): void {
    //
    const commentId = commentDto.usid;
    const targetComment = this.commentMap.get(commentId);

    if (!targetComment) {
      throw new Error('No such comment with id : ' + commentId);
    }

    if (commentDto.contents) {
      targetComment.contents = commentDto.contents;
    }

    const newComment = commentDto.toCommentIn(commentId, targetComment.postingId);

    this.commentMap.set(commentId, newComment);
  }

  remove(commentId: string): void {
    //
    if (!this.commentMap.get(commentId)) {
      throw new Error('No such comment with id --> ' + commentId);
    }
    this.commentMap.delete(commentId);
  }

}

export default CommentServiceLogic;
