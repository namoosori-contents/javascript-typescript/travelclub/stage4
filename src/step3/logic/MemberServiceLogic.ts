import CommunityMember from '../../step1/entity/club/CommunityMember';
import MemberDto from '../service/dto/MemberDto';
import MemberService from '../service/MemberService';
import MapStorage from './storage/MapStorage';
import ClubMembershipDto from '../service/dto/ClubMembershipDto';
import TravelClub from '../../step1/entity/club/TravelClub';
import ClubMembership from '../../step1/entity/club/ClubMembership';


class MemberServiceLogic implements MemberService {
  //
  clubMap: Map<string, TravelClub>;
  memberMap: Map<string, CommunityMember>;

  constructor() {
    //
    this.clubMap = MapStorage.getInstance().clubMap;
    this.memberMap = MapStorage.getInstance().memberMap;
  }

  register(memberDto: MemberDto): void {
    //
    const memberEmail = memberDto.email;

    const foundMember = this.memberMap.get(memberEmail);

    if (foundMember) {
      throw new Error('Member already exist the member email --> ' + foundMember.email);
    }
    this.memberMap.set(memberEmail, memberDto.toMember());
  }

  find(memberEmail: string): MemberDto {
    //
    const foundMember = this.memberMap.get(memberEmail);

    if (!foundMember) {
      throw new Error('No such member with email --> ' + memberEmail);
    }
    return MemberDto.fromEntity(foundMember);
  }

  findByName(memberName: string): MemberDto[] {
    //
    const members = Array.from(this.memberMap.values());

    return members.filter(member => member.name === memberName)
      .map(targetMember => MemberDto.fromEntity(targetMember));
  }

  modify(memberDto: MemberDto): void {
    //
    const memberEmail = memberDto.email;

    const targetMember = this.memberMap.get(memberEmail);

    if (!targetMember) {
      throw new Error('No such member with email --> ' + memberEmail);
    }

    if (!memberDto.name) {
      memberDto.name = targetMember.name;
    }

    if (!memberDto.nickName) {
      memberDto.nickName = targetMember.nickName;
    }

    if (!memberDto.phoneNumber) {
      memberDto.phoneNumber = targetMember.phoneNumber;
    }

    if (!memberDto.birthDay) {
      memberDto.birthDay = targetMember.birthDay;
    }

    this.memberMap.set(memberEmail, memberDto.toMember());
  }

  remove(memberEmail: string): void {
    //
    const foundMember = this.memberMap.get(memberEmail);

    if (!foundMember) {
      throw new Error('No such member with email --> ' + memberEmail);
    }

    const membershipListOfClub = this.findAllMemberships(memberEmail);

    this.removeAllMembershipsOfClub(memberEmail, membershipListOfClub);

    this.memberMap.delete(memberEmail);
  }

  findAllMemberships(memberEmail: string): ClubMembershipDto[] {
    //
    const memberships = this.memberMap.get(memberEmail)?.membershipList;

    if (!memberships) {
      return [];
    }

    return memberships.map(membership => ClubMembershipDto.fromEntity(membership));
  }

  removeAllMembershipsOfClub(memberId: string, clubMembershipDtos: ClubMembershipDto[]): void {
    //
    const clubs = clubMembershipDtos.map(membership => membership.clubId);

    for (const club of clubs) {
      const foundClub = this.clubMap.get(club);

      if (foundClub) {
        const targetMembership = foundClub.membershipList.find(membership => membership.memberEmail === memberId);

        foundClub.membershipList = foundClub.membershipList.filter(membership => membership !== targetMembership);
      }
    }
  }

  private getMembershipOfMember(clubId: string, member: CommunityMember): ClubMembership {
    //
    for (const membership of member.membershipList) {
      if (clubId === membership.clubId) {

        return membership;
      }
    }
    throw new Error(`No such club[${clubId}] in member [${member.email}]`);
  }

}

export default MemberServiceLogic;
